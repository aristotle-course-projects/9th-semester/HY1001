#
##############################################################################################################
# LabyrinthDB Roles
#############################################################################################################
#

#
# -----------------------------------------------------------------------------------------------------------
# STATISTICIAN Role
# ----------------------------------------------------------------------------------------------------------
#
# Create role STATISTICIAN that can perform the following queries in db:
#   - find number of total active users ( TOTAL_USERS_BY_SEX_VIEW )
#   - find number of total connections between users ( TOTAL_CONNECTIONS_BY_SEX_VIEW )
#   - find number of all messages sent between users of different sex ( TOTAL_MESSAGES_SENT_BY_SEX_VIEW )
#   - find number of all medias uploaded by users ( TOTAL_MEDIAS_UPLOADED_BY_SEX_VIEW )
#   - find number of all screenshots taken by users ( TOTAL_SCREENSHOTS_TAKEN_BY_SEX_VIEW )
#
CREATE ROLE 'lb_statistician';
GRANT SELECT ON labyrinth_db.TOTAL_USERS_BY_SEX_VIEW TO 'lb_statistician';
GRANT SELECT ON labyrinth_db.TOTAL_CONNECTIONS_BY_SEX_VIEW TO 'lb_statistician';
GRANT SELECT ON labyrinth_db.TOTAL_MESSAGES_SENT_BY_SEX_VIEW TO 'lb_statistician';
GRANT SELECT ON labyrinth_db.TOTAL_MEDIAS_UPLOADED_BY_SEX_VIEW TO 'lb_statistician';
GRANT SELECT ON labyrinth_db.TOTAL_SCREENSHOTS_TAKEN_BY_SEX_VIEW TO 'lb_statistician';

#
# -----------------------------------------------------------------------------------------------------------
# WEBAPP Role
# ----------------------------------------------------------------------------------------------------------
#
# Create role WEBAPP that can perform the following operations in db:
#   - SELECT, INSERT, UPDATE, DELETE on all tables
# 
# This role cannot add or delete database users.
#
CREATE ROLE 'lb_webapp';
GRANT SELECT, INSERT, UPDATE, DELETE ON labyrinth_db.* TO 'lb_webapp';

#
# -----------------------------------------------------------------------------------------------------------
# ADMIN Role
# ----------------------------------------------------------------------------------------------------------
#
# Create role ADMIN that can perform the following operations in db:
#   - ALL PRIVILIGES are given to this role
# 	- users granted this role can perform all action in labyrinth_db
# 
# This role cannot add or delete database users.
#
CREATE ROLE 'lb_admin';
GRANT ALL PRIVILEGES ON labyrinth_db.* TO 'lb_admin';
