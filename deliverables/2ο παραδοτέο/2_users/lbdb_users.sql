#
##############################################################################################################
# LabyrinthDB Users
#############################################################################################################
#

#
# -----------------------------------------------------------------------------------------------------------
# Statisticians
# ----------------------------------------------------------------------------------------------------------
#
# The following users are users that can perform several statistics-related queries to labyrinth db.
#   - they are granted the STATISTICIAN db role.
#   - they need not be accessing db from the same domain as where it is hosted.
#
CREATE USER 'data_analyst'@'%' IDENTIFIED BY 'data_analyst_password';
GRANT 'lb_statistician' TO 'data_analyst'@'%';

#
# -----------------------------------------------------------------------------------------------------------
# WebApp users
# ----------------------------------------------------------------------------------------------------------
#
# The following users are users that can perform the main operations to labyrinth db.
#   - they are granted the WEBAPP db role.
#   - they must be accessing db from the same domain as where it is hosted.
#
CREATE USER 'labyrinth_wau'@'localhost' IDENTIFIED BY 'labyrinth_wau_password';
GRANT 'lb_webapp' TO 'labyrinth_wau'@'localhost';

#
# -----------------------------------------------------------------------------------------------------------
# Administrators
# ----------------------------------------------------------------------------------------------------------
#
# The following users are users that can perform admin - maintance operations to labyrinth db.
#   - they are granted the ADMIN db role.
#   - they must be accessing db from the same domain as where it is hosted.
#
CREATE USER 'labyrinth_waa'@'localhost' IDENTIFIED BY 'labyrinth_waa_password';
GRANT 'lb_admin' TO 'labyrinth_waa'@'localhost';

