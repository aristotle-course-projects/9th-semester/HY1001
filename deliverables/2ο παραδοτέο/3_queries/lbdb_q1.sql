#
# --------------------------------------------
# Ερώτημα 1
# -------------------------------------------
#
# Περιγραφή:
# Έστω ότι επιθυμούμε για έναν συγκεκριμένο χρήστη ( π.χ. με id = 5 ) να δούμε τους προτεινόμενους σε αυτόν χρήστες από την εφαρμογή.
# Εκτελούμε το παρακάτω ερώτημα:
#
SELECT 
	suggestee_id
FROM
	suggestions
WHERE
	suggester_id = 5
;