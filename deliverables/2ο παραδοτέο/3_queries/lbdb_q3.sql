#
# --------------------------------------------
# Ερώτημα 3
# -------------------------------------------
#
# Περιγραφή:
# Έστω ότι επιθυμούμε να δούμε τους χρήστες που έκαναν screenshot σε συγκεκριμένο media ( π.χ. media με αριθμό id 5 ) ενός χρήστη.
# Εκτελούμε το παρακάτω ερώτημα:
#
SELECT 
	MEDIA_SCREENSHOTS_VIEW.screenshooter_id as screenshooter_id
FROM
	medias
    JOIN MEDIA_SCREENSHOTS_VIEW ON MEDIA_SCREENSHOTS_VIEW.media_id = medias.id
WHERE
	medias.id = 5
;
#
# Hint: Εάν θέλουμε να δούμε το αποτέλεσμα του αντίστοιχου ερωτήματος για ένα message αντί για ένα media ( αφού και τα δύο είναι screenshotable models ),
# απλώς αλλάζουμε την όψη - πίνακα του ερωτήματος από "MEDIA_SCREENSHOTS_VIEW" σε "MESSAGE_SCREENSHOTS_VIEW" ( υπάρχουν και οι δύο όψεις ).
#