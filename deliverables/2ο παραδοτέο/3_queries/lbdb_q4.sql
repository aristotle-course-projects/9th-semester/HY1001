#
# --------------------------------------------
# Ερώτημα 4
# -------------------------------------------
#
# Περιγραφή:
# Έστω ότι επιθυμούμε να δούμε τους χρήστες που έκαναν είτε “love” είτε “angry” σε ένα media ( π.χ. αυτού με id 5 ) κάποιου χρήστη.
# Εκτελούμε το παρακάτω ερώτημα:
#
SELECT 
	MEDIA_REACTIONS_VIEW.reactor_id as user_id
FROM
	medias
    JOIN MEDIA_REACTIONS_VIEW ON MEDIA_REACTIONS_VIEW.media_id = medias.id
WHERE
	medias.id = 5
    AND MEDIA_REACTIONS_VIEW.type IN ('love', 'sad')
;
#
# Hint: Εάν θέλουμε να δούμε το αποτέλεσμα του αντίστοιχου ερωτήματος για ένα message αντί για ένα media ( αφού και τα δύο είναι reactable models ),
# απλώς αλλάζουμε την όψη - πίνακα του ερωτήματος από "MEDIA_REACTIONS_VIEW" σε "MESSAGE_REACTIONS_VIEW" ( υπάρχουν και οι δύο όψεις ).
# Αντίστοιχα για ένα comment υπάρχει η όψη "COMMENT_REACTIONS_VIEW".
#