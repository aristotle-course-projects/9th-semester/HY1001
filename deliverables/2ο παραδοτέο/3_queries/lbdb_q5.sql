#
# --------------------------------------------
# Ερώτημα 5
# -------------------------------------------
#
# Περιγραφή:
# Έστω ότι θέλουμε να δούμε τα μηνύματα μεταξύ δύο χρηστών καθώς και τις ημερομηνίες στις οποίες στάλθηκαν ( υποθέτουμε ότι η συνομιλία είναι μεταξύ του χρήστη με id 3 και του χρήστη με id 5 ).
# Εκτελούμε το παρακάτω ερώτημα:
#
(
SELECT 
	id, sender_id, created_at
FROM
	messages
WHERE
	sender_id = 3
    AND receiver_id = 5
)
    
UNION

(
SELECT 
	id, sender_id, created_at
FROM
	messages
WHERE
	sender_id = 5
    AND receiver_id = 3
)

ORDER BY 
	created_at DESC
;
#
# Note: This way of displaying messages in a conversation is just to show a possible usage of union.
# A more practical way would be something like the following query:
# 
# SELECT 
# 	message_id, sender_id, receiver_id, created_at
# FROM
# 	messages
# WHERE
# 	(sender_id = 3 AND receiver_id = 5)
#   OR (sender_id = 5 AND receiver_id = 3)
# ORDER BY 
# 	created_at DESC
# ;
# 