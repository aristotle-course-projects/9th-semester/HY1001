-- MySQL dump 10.17  Distrib 10.3.11-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: 
-- ------------------------------------------------------
-- Server version	10.3.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `labyrinth_db`
--

DROP SCHEMA IF EXISTS `labyrinth_db`;
CREATE DATABASE `labyrinth_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `labyrinth_db`;

--
-- Temporary table structure for view `comment_reactions_view`
--

DROP TABLE IF EXISTS `comment_reactions_view`;
/*!50001 DROP VIEW IF EXISTS `comment_reactions_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `comment_reactions_view` (
  `id` tinyint NOT NULL,
  `reactor_id` tinyint NOT NULL,
  `comment_id` tinyint NOT NULL,
  `type` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_1_idx` (`media_id`),
  KEY `fk_comments_2_idx` (`author_id`),
  CONSTRAINT `fk_comments_1` FOREIGN KEY (`media_id`) REFERENCES `medias` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_2` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,61,4,'Aut minus numquam dolorem blanditiis beatae illum sed. Perferendis magni sequi voluptatibus in quam blanditiis. Eveniet molestiae doloribus temporibus amet veniam.','2018-12-22 22:32:18','2018-12-22 22:32:18'),(2,5,11,'Nesciunt temporibus omnis veniam harum eveniet voluptatum quam quam. Molestias exercitationem quo ut. Qui itaque placeat perspiciatis error. Delectus commodi asperiores et illum.','2018-12-22 22:32:18','2018-12-22 22:32:18'),(3,81,17,'Qui ducimus totam nam quo aliquid possimus. Est inventore animi omnis nostrum quidem suscipit ut. Doloribus molestiae sit aliquam corrupti earum.','2018-12-22 22:32:18','2018-12-22 22:32:18'),(4,84,13,'Ut ipsum voluptatum enim et. Aut pariatur adipisci praesentium nam dignissimos. Et dolorem totam eos consequatur et.','2018-12-22 22:32:18','2018-12-22 22:32:18'),(5,99,9,'Dolor est sit minus sed quis at dolores. Quod nobis recusandae et maiores corporis repudiandae.','2018-12-22 22:32:18','2018-12-22 22:32:18'),(6,62,7,'Excepturi et labore et quidem. Laborum pariatur dolores aspernatur omnis est sed possimus culpa. Itaque eius quo quia excepturi est aperiam consequatur exercitationem.','2018-12-22 22:32:18','2018-12-22 22:32:18'),(7,73,16,'Autem corrupti voluptatibus non reprehenderit. Et quibusdam veritatis dicta maxime. Consectetur provident itaque consequatur quam a. Cum nihil cupiditate voluptatibus ea aut.','2018-12-22 22:32:18','2018-12-22 22:32:18'),(8,36,8,'Et dolores vel reiciendis qui sit numquam. Sed quo sed sit odit porro. Architecto soluta mollitia veritatis quisquam facere.','2018-12-22 22:32:18','2018-12-22 22:32:18'),(9,63,17,'Consequatur ea ea optio iste id sit totam. Distinctio voluptatem molestias accusamus sed tempore. Similique exercitationem vero quod labore cum dolores consequatur.','2018-12-22 22:32:18','2018-12-22 22:32:18'),(10,31,15,'Est rem amet incidunt ipsa. Et esse ab rerum velit ab accusamus. Officiis doloribus at itaque hic autem. Ut optio architecto mollitia dolor assumenda ut.','2018-12-22 22:32:18','2018-12-22 22:32:18');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `connection_requests`
--

DROP TABLE IF EXISTS `connection_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connection_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requester_id` int(11) NOT NULL,
  `requestee_id` int(11) NOT NULL,
  `accepted_at` timestamp NULL DEFAULT NULL,
  `rejected_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_connection_requests_1_idx` (`requester_id`),
  KEY `fk_connection_requests_2_idx` (`requestee_id`),
  CONSTRAINT `fk_connection_requests_1` FOREIGN KEY (`requester_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_connection_requests_2` FOREIGN KEY (`requestee_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connection_requests`
--

LOCK TABLES `connection_requests` WRITE;
/*!40000 ALTER TABLE `connection_requests` DISABLE KEYS */;
INSERT INTO `connection_requests` VALUES (1,89,24,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(2,33,67,NULL,'2018-12-25 16:20:43','2018-12-22 22:32:18','2018-12-22 22:32:18'),(3,78,28,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(4,46,5,'2018-12-28 09:30:59',NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(5,52,50,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(6,69,13,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(7,71,19,'2018-12-30 08:11:54',NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(8,80,34,NULL,'2018-12-29 06:46:38','2018-12-22 22:32:18','2018-12-22 22:32:18'),(9,4,16,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(10,23,20,NULL,'2018-12-30 19:12:42','2018-12-22 22:32:18','2018-12-22 22:32:18'),(11,14,48,NULL,'2018-12-29 05:34:16','2018-12-22 22:32:18','2018-12-22 22:32:18'),(12,27,26,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(13,24,8,'2018-12-24 03:51:11',NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(14,54,26,NULL,'2019-01-01 14:59:19','2018-12-22 22:32:18','2018-12-22 22:32:18'),(15,89,63,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(16,27,12,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(17,4,38,'2018-12-28 19:05:23',NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(18,98,42,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(19,25,28,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(20,86,60,NULL,'2018-12-28 06:01:30','2018-12-22 22:32:18','2018-12-22 22:32:18'),(21,22,70,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(22,38,56,NULL,'2018-12-31 04:04:50','2018-12-22 22:32:18','2018-12-22 22:32:18'),(23,57,22,NULL,'2018-12-25 05:50:06','2018-12-22 22:32:18','2018-12-22 22:32:18'),(24,38,37,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(25,40,2,'2018-12-29 02:23:50',NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(26,42,3,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(27,84,48,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(28,3,55,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(29,80,16,NULL,NULL,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(30,46,19,NULL,'2018-12-27 11:13:19','2018-12-22 22:32:18','2018-12-22 22:32:18');
/*!40000 ALTER TABLE `connection_requests` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `labyrinth_db`.`connection_requests_BEFORE_INSERT` BEFORE INSERT ON `connection_requests` FOR EACH ROW
BEGIN
	IF ( NEW.requester_id = NEW.requestee_id ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: requester_id should be different from requestee_id';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `labyrinth_db`.`connection_requests_BEFORE_UPDATE` BEFORE UPDATE ON `connection_requests` FOR EACH ROW
BEGIN
	IF ( NEW.requester_id = NEW.requestee_id ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: requester_id should be different from requestee_id';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `connections`
--

DROP TABLE IF EXISTS `connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_connections_1_idx` (`request_id`),
  CONSTRAINT `fk_connections_1` FOREIGN KEY (`request_id`) REFERENCES `connection_requests` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connections`
--

LOCK TABLES `connections` WRITE;
/*!40000 ALTER TABLE `connections` DISABLE KEYS */;
INSERT INTO `connections` VALUES (1,7,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(2,3,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(3,1,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(4,29,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(5,19,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(6,16,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(7,8,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(8,21,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(9,26,'2018-12-22 22:32:18','2018-12-22 22:32:18'),(10,16,'2018-12-22 22:32:18','2018-12-22 22:32:18');
/*!40000 ALTER TABLE `connections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `media_comments`
--

DROP TABLE IF EXISTS `media_comments`;
/*!50001 DROP VIEW IF EXISTS `media_comments`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `media_comments` (
  `id` tinyint NOT NULL,
  `body` tinyint NOT NULL,
  `created_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `media_reactions_view`
--

DROP TABLE IF EXISTS `media_reactions_view`;
/*!50001 DROP VIEW IF EXISTS `media_reactions_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `media_reactions_view` (
  `id` tinyint NOT NULL,
  `reactor_id` tinyint NOT NULL,
  `media_id` tinyint NOT NULL,
  `type` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `media_screenshots_view`
--

DROP TABLE IF EXISTS `media_screenshots_view`;
/*!50001 DROP VIEW IF EXISTS `media_screenshots_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `media_screenshots_view` (
  `id` tinyint NOT NULL,
  `screenshooter_id` tinyint NOT NULL,
  `media_id` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `medias`
--

DROP TABLE IF EXISTS `medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploader_id` int(10) unsigned NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `type` enum('picture','video','audio') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medias`
--

LOCK TABLES `medias` WRITE;
/*!40000 ALTER TABLE `medias` DISABLE KEYS */;
INSERT INTO `medias` VALUES (1,59,'0','audio','2018-12-22 22:32:16','2018-12-22 22:32:16'),(2,62,'0','video','2018-12-22 22:32:16','2018-12-22 22:32:16'),(3,97,'0','picture','2018-12-22 22:32:16','2018-12-22 22:32:16'),(4,81,'0','picture','2018-12-22 22:32:16','2018-12-22 22:32:16'),(5,69,'0','picture','2018-12-22 22:32:16','2018-12-22 22:32:16'),(6,74,'0','picture','2018-12-22 22:32:16','2018-12-22 22:32:16'),(7,98,'0','audio','2018-12-22 22:32:16','2018-12-22 22:32:16'),(8,80,'0','video','2018-12-22 22:32:16','2018-12-22 22:32:16'),(9,76,'0','video','2018-12-22 22:32:16','2018-12-22 22:32:16'),(10,86,'0','picture','2018-12-22 22:32:16','2018-12-22 22:32:16'),(11,44,'0','picture','2018-12-22 22:32:16','2018-12-22 22:32:16'),(12,77,'0','picture','2018-12-22 22:32:16','2018-12-22 22:32:16'),(13,26,'0','audio','2018-12-22 22:32:16','2018-12-22 22:32:16'),(14,16,'0','audio','2018-12-22 22:32:16','2018-12-22 22:32:16'),(15,57,'0','audio','2018-12-22 22:32:16','2018-12-22 22:32:16'),(16,45,'0','video','2018-12-22 22:32:16','2018-12-22 22:32:16'),(17,44,'0','video','2018-12-22 22:32:16','2018-12-22 22:32:16'),(18,44,'0','audio','2018-12-22 22:32:16','2018-12-22 22:32:16'),(19,52,'0','video','2018-12-22 22:32:16','2018-12-22 22:32:16'),(20,68,'0','picture','2018-12-22 22:32:17','2018-12-22 22:32:17');
/*!40000 ALTER TABLE `medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `message_reactions_view`
--

DROP TABLE IF EXISTS `message_reactions_view`;
/*!50001 DROP VIEW IF EXISTS `message_reactions_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `message_reactions_view` (
  `id` tinyint NOT NULL,
  `reactor_id` tinyint NOT NULL,
  `message_id` tinyint NOT NULL,
  `type` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `message_screenshots_view`
--

DROP TABLE IF EXISTS `message_screenshots_view`;
/*!50001 DROP VIEW IF EXISTS `message_screenshots_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `message_screenshots_view` (
  `id` tinyint NOT NULL,
  `screenshooter_id` tinyint NOT NULL,
  `message_id` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `body` text DEFAULT NULL,
  `type` enum('text','media','mixed') NOT NULL,
  `delivered_at` timestamp NULL DEFAULT NULL,
  `viewed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_messages_1_idx` (`sender_id`),
  KEY `fk_messages_2_idx` (`media_id`),
  KEY `fk_messages_3_idx` (`receiver_id`),
  CONSTRAINT `fk_messages_1` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_2` FOREIGN KEY (`media_id`) REFERENCES `medias` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_3` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,1,65,NULL,'Qui quidem veritatis non non porro necessitatibus. Velit quasi odio mollitia. Omnis quia qui pariatur. Et ipsam sint laudantium vel reprehenderit odio.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(2,58,73,NULL,'Voluptatem ut optio dolorem praesentium molestias ullam fugiat. Doloremque odio adipisci error dolorem dolore expedita.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(3,57,80,NULL,'Aut est velit dicta velit qui eveniet sunt. Similique odit quaerat neque iusto officia. Maiores harum ut consequatur non.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(4,2,74,NULL,'Omnis sunt autem aliquid nobis. Veritatis minus quo omnis et. Iure magnam vitae voluptas officiis earum autem. Vero odio consequatur illo qui nulla autem.','mixed','2018-11-25 19:04:49','2018-12-03 13:26:37','2018-10-31 20:27:56','2018-12-03 13:26:37'),(5,14,84,NULL,'Aperiam earum temporibus natus accusamus. Aut et nulla est sed perferendis voluptas exercitationem.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(6,25,86,NULL,'Quaerat nulla sit molestias. Aut quasi aperiam voluptas veniam. Quo qui sit error dicta. Vel voluptatem sunt dolorum.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(7,43,79,17,'Neque facere est ab quidem dicta quas velit. Voluptatibus quis optio aperiam est asperiores eos atque. Animi culpa odio veniam quo laudantium asperiores repellat.','media','2018-12-20 09:18:49','2018-12-20 12:59:26','2018-10-24 16:53:50','2018-12-20 12:59:26'),(8,37,89,NULL,'Expedita voluptatem porro et natus vero eligendi sint. Ad officia in ea sit.','text','2018-11-26 19:31:12','2018-12-13 06:15:58','2018-11-21 06:19:48','2018-12-13 06:15:58'),(9,38,53,17,'Consequatur incidunt earum neque. Sunt aut non animi animi. Molestias autem unde doloribus. Eum dolorem dolorum et corrupti possimus.','media','2018-12-06 15:42:00','2018-12-18 04:10:59','2018-09-24 14:19:05','2018-12-18 04:10:59'),(10,59,72,10,'Ut voluptates nobis repellat. Aliquid ipsum assumenda corrupti sed. Ut incidunt temporibus veritatis illum et quas eaque. Quia quis ut quas.','media','2018-12-08 03:15:15','2018-12-22 02:48:19','2018-10-30 04:31:09','2018-12-22 02:48:19'),(11,28,63,NULL,'Qui eaque ea possimus aliquam et rem. Architecto quam corrupti qui aut. Aut nemo voluptatem dicta sed impedit.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(12,70,88,NULL,'Qui in perferendis consequatur animi eius ut explicabo. Necessitatibus quasi consectetur omnis culpa vel ducimus minima rem. Voluptatum vel magni dolor qui. Sapiente eum qui sequi ab vitae eum.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(13,10,39,NULL,'Sequi nostrum porro suscipit quis. Velit ipsum et esse voluptate. Tempora enim doloremque corporis ullam. Occaecati quos optio nostrum expedita. Ea iusto aspernatur praesentium ex.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(14,18,78,6,'Corrupti vero et modi porro. Laudantium nostrum eos quo praesentium dolor recusandae et deleniti. Nihil ducimus rerum quia nihil amet. Odit animi quae dolorem ratione dolores aut omnis.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(15,20,21,NULL,'Molestias inventore consequatur sunt est ut aut. Dolor saepe necessitatibus dolore. Totam quis qui et laborum reiciendis amet.','text','2018-12-06 18:22:29','2018-12-12 22:38:34','2018-10-27 17:13:34','2018-12-12 22:38:34'),(16,31,54,17,'Quaerat sed dolorem aliquam perferendis temporibus. Eaque placeat corporis delectus et. Laboriosam similique deleniti dolores dolor odit velit consectetur. Molestiae aut aliquam atque expedita.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(17,46,93,NULL,'Voluptatum consectetur harum aliquam ab consectetur aut. In aut ut et sunt rerum qui doloremque. A quo sit asperiores inventore officiis voluptatum quod. Animi nisi quasi quasi quae ad ut et.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(18,28,29,NULL,'Nisi accusamus reiciendis sunt dolorem perspiciatis distinctio non. Et reiciendis possimus et aliquam velit. Ea autem enim doloremque cum non unde repellendus. Voluptatem blanditiis sequi quae id.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(19,18,92,NULL,'In adipisci quis id. Accusantium sequi cumque eum at. Ab quia incidunt qui voluptates saepe voluptatem. Consequuntur repellat ea placeat eum animi.','mixed','2018-12-02 00:42:11','2018-12-05 11:02:11','2018-11-02 06:33:20','2018-12-05 11:02:11'),(20,11,95,NULL,'Voluptas numquam modi libero reprehenderit sunt expedita dolores qui. Enim perspiciatis rem voluptatem quam. Autem sequi et facere quaerat qui aliquid quod.','text','2018-11-30 13:46:02','2018-12-15 12:31:27','2018-09-25 06:27:39','2018-12-15 12:31:27'),(21,45,65,NULL,'Dolores enim eos aliquam voluptatem aperiam. Laboriosam ipsam omnis dolorem eveniet recusandae veritatis. Perspiciatis harum ut ex. Cum architecto consectetur occaecati quia ducimus fuga nemo.','text','2018-12-16 07:02:32','2018-12-22 20:51:25','2018-10-18 14:23:10','2018-12-22 20:51:25'),(22,23,29,NULL,'Ea molestiae earum beatae debitis eligendi molestias aliquid. Aut quia sed nisi sed eius consectetur vel. Incidunt eum sed sunt enim labore tempore natus.','text','2018-12-08 13:20:53','2018-12-17 18:33:11','2018-10-04 19:07:34','2018-12-17 18:33:11'),(23,22,63,NULL,'Velit tempore ut ab et vero ipsum quas. Eos doloremque tempora suscipit optio sint aut. Impedit similique dolore ab eligendi labore sit. Ea molestias cupiditate quis illo.','mixed','2018-12-13 05:44:26','2018-12-16 15:44:40','2018-10-01 12:23:24','2018-12-16 15:44:40'),(24,56,81,13,'Minima rerum dolorum quae officiis. Ut molestiae qui animi consequatur. Quis fuga velit corrupti sint deserunt excepturi. Veritatis deserunt aut nesciunt eius est ab nulla.','media','2018-12-02 06:23:48','2018-12-09 21:11:38','2018-11-22 17:45:12','2018-12-09 21:11:38'),(25,9,12,1,'Officia culpa error dolor molestias. Commodi occaecati ea quas adipisci aliquam. Praesentium laudantium non qui dolores. Esse in sed recusandae aut unde rem.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(26,70,71,10,'Ipsum qui eos vero. Voluptas possimus dolores necessitatibus architecto dolorum. Distinctio dicta corrupti molestiae voluptatem reprehenderit.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(27,10,42,NULL,'Tempora odio laborum sunt aut eius. Sed minus facere facilis beatae praesentium. Repellendus officia et vero.','text','2018-12-07 12:30:57','2018-12-12 07:40:17','2018-11-18 13:12:07','2018-12-12 07:40:17'),(28,59,85,NULL,'Officiis aspernatur error harum rem nemo numquam. Fugiat harum eos corrupti eligendi ipsum occaecati temporibus. Illum odio aut harum incidunt.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(29,39,52,NULL,'Possimus aut quisquam consequatur commodi debitis quae voluptas. Veritatis laboriosam officia voluptas perferendis sed pariatur. Pariatur laudantium quae architecto aut perferendis magnam.','mixed','2018-12-04 06:29:43','2018-12-04 23:46:08','2018-11-18 16:32:47','2018-12-04 23:46:08'),(30,33,92,NULL,'Ut et soluta qui soluta quaerat. Est iusto dolor voluptatibus voluptas ipsum. Molestiae autem est ut eum. Dolor voluptas quod occaecati voluptatem sunt sint.','mixed','2018-12-19 19:59:31','2018-12-20 18:37:33','2018-11-07 21:05:05','2018-12-20 18:37:33'),(31,22,90,NULL,'Nobis recusandae molestiae qui ea molestiae. Iste et doloremque dolorem quam sunt et. Quibusdam quia vel id eos adipisci tenetur. Quisquam illo sapiente et harum tenetur quibusdam.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(32,63,99,17,'Et hic eum rem voluptates quisquam. Qui omnis aperiam temporibus itaque sed neque molestiae. Quia accusamus esse laudantium repellendus et dolorem excepturi.','media','2018-12-12 16:14:36','2018-12-18 20:16:58','2018-11-09 20:02:15','2018-12-18 20:16:58'),(33,56,73,NULL,'Ut et earum tempora alias. Qui nihil sunt est aut quidem velit eligendi. Ullam dolorem quas eos et minima velit voluptas. Aut nisi dolorem qui aperiam odio distinctio voluptatem.','mixed','2018-11-29 13:02:15','2018-12-13 21:19:09','2018-11-19 07:32:08','2018-12-13 21:19:09'),(34,40,81,NULL,'Minus deleniti maiores saepe id ipsa illum. Iure debitis aut autem sed minima qui.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(35,15,62,4,'Aut reiciendis deserunt dolore expedita soluta et occaecati consectetur. Ut quasi ipsam quia cum assumenda quo. Eligendi vero consectetur similique modi ea. Et doloremque blanditiis et numquam.','media','2018-11-24 21:43:53','2018-12-01 17:50:28','2018-10-07 22:04:37','2018-12-01 17:50:28'),(36,69,76,NULL,'Non sequi quia qui. Molestiae animi et saepe occaecati ipsa deserunt cum. Ab possimus nobis tempore necessitatibus sunt velit ut. Dolores dolor sit sit aut corrupti architecto autem.','mixed','2018-12-15 20:27:29','2018-12-19 06:26:54','2018-10-13 03:58:31','2018-12-19 06:26:54'),(37,29,88,NULL,'Doloribus consequuntur omnis eos iusto alias. Ea fuga provident eveniet et adipisci vitae autem. Veniam aliquam optio error eveniet qui consequatur atque. Et provident fugiat velit sed impedit et.','text','2018-12-03 21:29:25','2018-12-07 07:12:05','2018-09-26 23:54:06','2018-12-07 07:12:05'),(38,31,93,NULL,'Consequatur ea quo quia quo. Minima ipsa est voluptas consequatur non. Aut et ratione ut.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(39,16,40,NULL,'Eum modi autem et dolorem aut sequi repellendus. Quaerat beatae qui qui in rerum porro eos provident. Accusamus nihil veritatis voluptas nesciunt. Eius aut laboriosam et eos omnis.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(40,63,84,NULL,'Cumque et quis at. Quia non nobis animi facere asperiores explicabo quis. Sit autem adipisci ab quam architecto voluptatibus.','text','2018-12-10 07:32:25','2018-12-12 10:54:05','2018-10-08 15:36:53','2018-12-12 10:54:05'),(41,1,4,NULL,'Adipisci tempore sint nesciunt quia. Nostrum suscipit sit expedita iure laudantium ipsa nam. Harum aut pariatur minima quis vitae rem pariatur.','mixed','2018-11-26 07:38:52','2018-12-18 06:44:13','2018-11-12 19:54:47','2018-12-18 06:44:13'),(42,46,53,NULL,'Ea vel nam blanditiis iste quibusdam libero similique omnis. Fugit fugit sapiente est id facilis facere quo. Cupiditate voluptatibus ut cum vitae aspernatur pariatur ut.','text','2018-11-29 03:42:07','2018-12-05 15:27:01','2018-10-28 02:03:57','2018-12-05 15:27:01'),(43,37,69,14,'Dignissimos qui totam neque eveniet sequi et unde. Eos sint expedita voluptas ipsam velit non. Ut cupiditate rerum vero. Tempore aut voluptatibus sit.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(44,60,92,NULL,'Est ut qui vel. Rerum asperiores molestias doloribus dolorum. Possimus quisquam voluptas dolor alias.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(45,45,96,1,'Provident asperiores natus laborum temporibus laudantium quae. Dicta voluptatem accusamus eius eum ut ipsam. Corporis culpa mollitia perferendis quidem ad quia. Dolores veniam et expedita mollitia.','media','2018-12-14 04:24:36','2018-12-19 08:58:10','2018-10-20 17:35:46','2018-12-19 08:58:10'),(46,26,88,NULL,'Nobis autem qui asperiores officia cumque quas eum autem. Et doloribus et sit magni commodi pariatur commodi. Repudiandae et eaque culpa pariatur ut ut saepe.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(47,13,25,NULL,'Debitis ut voluptatum reiciendis est et quis ut. Aut perferendis nihil voluptas a vero quos. Consequatur blanditiis quia at et. Quia et nihil et quam dolores.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(48,56,67,8,'Accusantium iste vel non doloremque natus deleniti reprehenderit. Molestiae aut aut eum tempora soluta sed ad. Non molestiae possimus cupiditate ut vitae aut eveniet. Consequatur quaerat et aut.','media','2018-12-12 20:28:16','2018-12-17 11:19:39','2018-09-29 17:46:34','2018-12-17 11:19:39'),(49,48,78,NULL,'Aliquid optio commodi voluptates illo. Tempore porro ea aliquam molestiae. Voluptatum voluptatem quisquam aut quasi vel autem a.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(50,56,79,12,'Consectetur omnis perferendis repudiandae porro. Odio recusandae laudantium harum esse.','media','2018-11-29 06:47:40','2018-11-29 17:11:04','2018-10-13 18:52:43','2018-11-29 17:11:04'),(51,60,83,NULL,'Labore voluptate sequi quidem incidunt consequatur qui. Omnis velit voluptas expedita facilis. Hic eos voluptate veniam fugiat quis enim modi. Aut vitae ut dicta quo minima.','text','2018-11-23 11:54:52','2018-12-01 21:26:25','2018-11-07 17:47:49','2018-12-01 21:26:25'),(52,44,88,NULL,'Facilis a alias animi expedita sint. Consequuntur recusandae veritatis veritatis sint. Tenetur recusandae ipsum ipsum. Consectetur quia pariatur repellat. Quasi earum velit eos eaque.','mixed','2018-12-19 10:15:12','2018-12-20 16:21:59','2018-11-15 08:45:00','2018-12-20 16:21:59'),(53,35,64,NULL,'Minima soluta eveniet quae doloribus ducimus voluptatem et. Nemo voluptatem quia deleniti accusamus blanditiis. In ab aspernatur sunt consequatur.','mixed','2018-11-26 04:33:56','2018-11-28 09:38:38','2018-10-12 18:32:42','2018-11-28 09:38:38'),(54,63,72,NULL,'Facilis nam veniam ad adipisci blanditiis. Unde sapiente itaque pariatur dolor. Velit officia perferendis nesciunt hic perspiciatis. Pariatur soluta ab dolor voluptas quis.','text','2018-12-12 07:16:11','2018-12-20 02:13:57','2018-10-16 19:51:33','2018-12-20 02:13:57'),(55,54,82,NULL,'Commodi earum ab aut veritatis. Perferendis ut ut rerum unde consequatur.','mixed','2018-12-05 04:35:16','2018-12-12 06:00:26','2018-10-02 22:23:15','2018-12-12 06:00:26'),(56,21,52,NULL,'Corrupti error itaque eligendi consectetur amet. Dolorem vero ipsam et. Consequatur ullam nesciunt laborum excepturi quidem dolor. Voluptate at culpa est labore eaque quaerat consectetur.','mixed','2018-11-25 20:39:50','2018-12-22 20:39:00','2018-10-19 05:52:28','2018-12-22 20:39:00'),(57,34,65,NULL,'Aspernatur est nihil voluptate qui. Et veniam possimus omnis placeat quia inventore. Laboriosam magnam quam nam non facere.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(58,9,98,NULL,'Dolor nemo soluta et sequi est nihil saepe. Aliquid saepe temporibus beatae accusantium quibusdam rerum libero. Occaecati id odit dolor nesciunt.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(59,39,100,NULL,'Aliquam facilis incidunt quae iusto reiciendis voluptatibus. Officia repudiandae doloremque tempore maxime quidem sequi.','text','2018-12-04 16:15:04','2018-12-15 02:08:19','2018-10-14 13:30:56','2018-12-15 02:08:19'),(60,70,82,NULL,'Exercitationem ex dignissimos qui porro tenetur quia qui. Placeat et quod dolor similique et rerum sunt. Esse architecto explicabo vero ducimus atque officia cupiditate voluptatem.','text','2018-12-19 12:03:02','2018-12-19 18:32:59','2018-10-13 05:29:58','2018-12-19 18:32:59'),(61,7,42,10,'Cupiditate molestiae esse laudantium veritatis. Est minima et reprehenderit perspiciatis. Facilis voluptatibus iure soluta.','media','2018-11-29 18:39:55','2018-12-21 00:46:09','2018-09-27 23:57:09','2018-12-21 00:46:09'),(62,35,68,2,'Ipsum velit voluptatem ratione magni voluptates iste aut. Nobis sint repellendus accusamus consequatur est. Omnis et quisquam quibusdam totam porro. Fugit qui voluptatem voluptatum facere quasi.','media','2018-12-16 21:57:35','2018-12-21 13:51:52','2018-09-27 14:59:59','2018-12-21 13:51:52'),(63,63,71,NULL,'Expedita praesentium suscipit et cum quae. Aut nihil dolores reprehenderit. Vel velit sint voluptatem nam quas quis.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(64,12,39,NULL,'Fugiat qui harum vero. Doloribus a omnis omnis accusamus quidem consequatur ex. Nulla aperiam fugit sint quod.','mixed','2018-12-13 12:50:57','2018-12-20 19:12:00','2018-10-08 16:57:07','2018-12-20 19:12:00'),(65,15,29,17,'Animi quae eum velit consequuntur optio. Dolores aut voluptas commodi reiciendis quia. Dolores repellat nam sed eligendi quo dolorem.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(66,63,94,9,'Ea excepturi aut enim iure sit delectus sit. Culpa id officia similique eos. Quaerat est sit odio deserunt. Illo nesciunt nemo est rerum saepe.','media','2018-12-03 17:02:27','2018-12-18 08:47:54','2018-10-23 00:42:53','2018-12-18 08:47:54'),(67,64,75,NULL,'Tenetur rerum aut id ut odio. Voluptatum eius qui quia voluptas dolores assumenda vel. In expedita sit assumenda. A et voluptatibus voluptas deleniti quia.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(68,12,39,NULL,'Maiores aut molestias consequatur dolor adipisci repellendus. Harum expedita quos dolorem. Ut quia dolore nostrum voluptatem eius et.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(69,28,91,14,'Maxime unde omnis sunt ad. Praesentium nihil mollitia ab voluptatem eos fugiat maiores.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(70,34,64,16,'Nostrum odio reprehenderit nemo quia qui. Placeat illum natus aut consequatur qui. Ut necessitatibus dolore laudantium assumenda. Perferendis dolorum dolorem et repudiandae.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(71,38,76,NULL,'Ea officia repellendus ex rerum. Velit temporibus ab qui consequatur.','mixed','2018-11-25 19:14:16','2018-11-29 02:41:52','2018-09-28 05:50:06','2018-11-29 02:41:52'),(72,48,80,NULL,'Porro debitis quia earum amet nihil. Ipsa voluptate molestiae alias qui et rerum. Ut repellendus saepe quia molestias.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(73,40,46,NULL,'Voluptas ea molestiae amet dolorem at sint. Rerum autem sunt iste qui. Fugit quisquam eos dolore minima ut. Quia soluta a itaque et.','text','2018-12-05 09:25:19','2018-12-12 23:07:25','2018-11-02 23:05:41','2018-12-12 23:07:25'),(74,13,64,NULL,'Architecto et ea ut est. Fugiat non quae debitis voluptatibus doloribus. Est sunt libero enim.','text','2018-12-12 11:18:36','2018-12-14 17:08:30','2018-09-27 03:49:04','2018-12-14 17:08:30'),(75,9,15,NULL,'Ducimus nobis eos voluptas modi sequi voluptatem. Tenetur nesciunt veniam voluptas quidem autem. Nihil officiis quis ut facere pariatur.','text','2018-12-20 22:51:03','2018-12-21 12:34:03','2018-09-27 04:49:50','2018-12-21 12:34:03'),(76,3,11,19,'Non sit in dolores voluptate. Quam quia vero blanditiis ea accusamus sunt. Rerum non veniam dolores ab voluptas fugit est. Sint quod ut dicta minima et molestiae maiores.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(77,46,78,15,'Qui perspiciatis animi magnam quidem dolor quibusdam dolorem perspiciatis. Possimus ut iste qui. In molestiae labore labore et quis vero eaque.','media','2018-12-21 10:39:47','2018-12-21 21:46:00','2018-10-26 05:35:26','2018-12-21 21:46:00'),(78,13,61,10,'Reiciendis tempore blanditiis ipsum. Soluta culpa saepe recusandae ratione. Et voluptatem quia sint.','media','2018-12-09 17:36:46','2018-12-13 02:15:28','2018-10-10 19:49:12','2018-12-13 02:15:28'),(79,48,85,12,'Fugiat et saepe expedita sed. Rerum ut deleniti est ipsam. Quia minus rerum autem voluptates dolores vero.','media','2018-12-21 18:02:42','2018-12-21 20:05:25','2018-10-02 17:04:58','2018-12-21 20:05:25'),(80,24,69,NULL,'Quia dolores autem minus aut architecto. Vel suscipit assumenda sequi repudiandae similique optio. Velit similique voluptas culpa pariatur qui minima est.','mixed','2018-12-07 10:32:49','2018-12-07 20:24:22','2018-11-13 12:02:21','2018-12-07 20:24:22'),(81,4,35,NULL,'Odit harum eos omnis autem non et nemo. Assumenda et sunt placeat aliquid expedita nemo. Minima ullam minus neque ab itaque consequatur delectus.','text','2018-12-09 15:57:54','2018-12-11 21:24:44','2018-10-19 16:32:55','2018-12-11 21:24:44'),(82,65,79,NULL,'Sequi officiis dolores repellendus qui odit ratione aut. Ducimus commodi est cum aliquid provident ut. Eum voluptatem et repudiandae amet dolorem deserunt iure.','text','2018-11-28 19:10:20','2018-12-09 18:55:56','2018-10-31 15:38:13','2018-12-09 18:55:56'),(83,49,56,NULL,'Numquam omnis quaerat aliquid vero qui dignissimos error. Quo dolores rerum ut facere amet excepturi. Voluptatem maiores optio non et et voluptatem alias. Facere ut quia incidunt vel.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(84,45,97,NULL,'Rerum maiores tempore unde dolores. Accusantium architecto ut repellendus. Quos doloribus mollitia at dolorum in voluptatem id. Beatae eos hic sit ut quae nulla dolorum.','text','2018-12-08 08:02:03','2018-12-11 13:17:37','2018-10-04 23:11:18','2018-12-11 13:17:37'),(85,28,44,NULL,'Reiciendis voluptatem quia enim dolore qui. Ea omnis saepe nobis reiciendis fugiat. Quis assumenda eaque iusto tempora quaerat autem magni.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(86,39,92,NULL,'Ex architecto et deleniti. Iure earum qui voluptatem tenetur magnam. Sit alias nihil quo accusamus sunt omnis quia.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(87,29,35,NULL,'Adipisci voluptatem sit id quae. Earum pariatur dolorem suscipit minima excepturi. Rerum ea non saepe est.','mixed',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(88,11,12,7,'Et ab exercitationem quod praesentium. Sit in dolores nisi et qui. Dolores voluptas ut atque eligendi cupiditate laboriosam in ipsum.','media','2018-11-29 21:29:30','2018-12-22 20:07:39','2018-09-30 03:02:00','2018-12-22 20:07:39'),(89,8,34,NULL,'Perferendis maxime possimus et. Enim non voluptatum non odio quidem dolorem soluta. Similique inventore id facere aliquam et sit placeat.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(90,32,45,NULL,'Quibusdam a nostrum minima enim debitis cum dolor. Reiciendis molestiae voluptatem facere dolores in. Molestiae nam occaecati nihil. Aut dolorem a fuga id quis deserunt.','mixed','2018-11-25 19:14:16','2018-12-22 12:42:25','2018-10-25 08:02:41','2018-12-22 12:42:25'),(91,22,91,NULL,'Est exercitationem quas voluptas aut est. Iusto numquam debitis commodi delectus velit odit dolorum. Est id harum omnis cumque quia. Ut quo in aut.','text','2018-12-19 21:40:44','2018-12-20 22:12:44','2018-10-12 15:22:39','2018-12-20 22:12:44'),(92,18,73,NULL,'Ut distinctio et et possimus voluptas. Et dicta voluptatum vel voluptas animi porro nihil. Eum sit maiores maxime ipsum eaque odit velit sint. Placeat id ducimus dicta sed ab quibusdam vel.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(93,59,80,NULL,'Nulla officiis ullam atque eos modi. Ipsam eligendi ullam sed velit. Qui deleniti vel est nobis.','text','2018-12-20 17:12:15','2018-12-21 09:07:49','2018-09-29 10:19:39','2018-12-21 09:07:49'),(94,15,34,14,'Modi totam numquam quidem vel. Ut perferendis et natus est. Perferendis fugiat enim perspiciatis sed.','media',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17'),(95,46,90,14,'Est repellendus provident et beatae. Incidunt saepe aut rerum et in reprehenderit. Odio dolore corrupti dolores modi consectetur beatae reiciendis.','media','2018-11-29 12:28:39','2018-12-06 01:19:31','2018-10-29 02:43:45','2018-12-06 01:19:31'),(96,21,43,NULL,'Architecto et qui et vel. Aut quisquam reiciendis earum quia ea.','mixed','2018-12-12 08:38:31','2018-12-14 19:24:14','2018-11-09 12:31:08','2018-12-14 19:24:14'),(97,68,94,18,'Minus odio culpa saepe molestiae. Rerum harum non tempora incidunt facere. Aliquam sit blanditiis ullam mollitia dignissimos quaerat. Est odit quo pariatur exercitationem aspernatur quaerat tempora.','media','2018-11-27 18:44:42','2018-12-21 16:15:31','2018-10-25 20:01:49','2018-12-21 16:15:31'),(98,60,69,NULL,'Eveniet earum quo voluptatibus aliquam in. Dolorum quo sint quo possimus sed commodi sit. Ratione quis fugit animi cumque qui. Laboriosam quibusdam sequi ipsam.','mixed','2018-12-02 13:45:28','2018-12-14 05:29:44','2018-10-06 15:00:41','2018-12-14 05:29:44'),(99,49,71,14,'Non cupiditate est qui neque. Doloribus et veritatis ipsam. Ut et nisi quia nostrum. Non sit ex vitae rem. Et quod ducimus aut. Et est quod maiores ut fugiat facere illo.','media','2018-12-10 00:05:28','2018-12-22 16:31:01','2018-10-17 20:51:52','2018-12-22 16:31:01'),(100,50,94,NULL,'Dolores ut accusamus ea molestiae eaque ea. Nihil qui omnis non. Ipsam dignissimos sit reprehenderit et error reiciendis et. Odit sit optio nihil consectetur accusamus numquam reprehenderit qui.','text',NULL,NULL,'2018-12-22 22:32:17','2018-12-22 22:32:17');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `labyrinth_db`.`messages_BEFORE_INSERT` BEFORE INSERT ON `messages` FOR EACH ROW
BEGIN
	IF ( NEW.sender_id = NEW.receiver_id ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: sender_id should be different from receiver_id';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `labyrinth_db`.`messages_BEFORE_UPDATE` BEFORE UPDATE ON `messages` FOR EACH ROW
BEGIN
	IF ( NEW.sender_id = NEW.receiver_id ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: sender_id should be different from receiver_id';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `notification_category`
--

DROP TABLE IF EXISTS `notification_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_category` (
  `notification_id` int(11) NOT NULL,
  `category` varchar(45) NOT NULL,
  PRIMARY KEY (`notification_id`,`category`),
  CONSTRAINT `fk_notification_category_1` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_category`
--

LOCK TABLES `notification_category` WRITE;
/*!40000 ALTER TABLE `notification_category` DISABLE KEYS */;
INSERT INTO `notification_category` VALUES (1,'suggestions'),(2,'connections'),(2,'maintenance'),(2,'suggestions'),(3,'maintenance'),(4,'maintenance'),(4,'suggestions'),(5,'app'),(5,'suggestions'),(6,'app'),(6,'connections'),(6,'suggestions'),(7,'app'),(7,'connections'),(7,'maintenance'),(8,'app'),(8,'connections'),(8,'maintenance'),(9,'maintenance'),(10,'app'),(10,'connections'),(10,'suggestions');
/*!40000 ALTER TABLE `notification_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_id` int(11) NOT NULL,
  `body` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `type` enum('prompt','info','urgent') NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notifications_1_idx` (`to_id`),
  CONSTRAINT `fk_notifications_1` FOREIGN KEY (`to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,64,'Est nihil vel odio. Nemo vitae dolorem doloremque hic laudantium. Quo quae dolores nam culpa illo. Aut eum aliquid cumque qui quia officia.','http://hodkiewicz.com/','urgent','2018-12-16 03:30:08','2018-12-22 22:32:13','2018-12-22 22:32:13'),(2,2,'Ut quas rem nesciunt sit quia aliquam autem. Eius corporis nam eveniet qui. Non magni et beatae et quae illum. Ut aperiam dolorum optio et architecto libero cum.','http://cassin.com/eos-illo-nihil-deserunt-temporibus-et','urgent',NULL,'2018-12-22 22:32:13','2018-12-22 22:32:13'),(3,56,'Dolore corrupti tempore modi incidunt magnam ratione. Temporibus delectus nihil perspiciatis eius. Voluptatem minus nemo atque dolorem consequuntur rerum.','http://www.huels.info/quidem-repellat-ut-illo-tempore','prompt','2018-12-17 11:16:25','2018-12-22 22:32:13','2018-12-22 22:32:13'),(4,8,'Vitae earum et minima consequatur aut voluptas. Maiores et recusandae iusto sint a. Blanditiis quos earum esse error ducimus odit hic. Quia qui quis et cupiditate aperiam.','http://www.larkin.com/et-dolorem-asperiores-delectus','info',NULL,'2018-12-22 22:32:13','2018-12-22 22:32:13'),(5,87,'Alias quae suscipit non quia. Sit iure mollitia molestiae cupiditate neque consequuntur molestias. Temporibus rem est laudantium molestiae alias. Quis eos sint laudantium sequi qui voluptatibus et.','https://www.collins.info/nostrum-cumque-numquam-voluptas-nobis-quo','urgent',NULL,'2018-12-22 22:32:13','2018-12-22 22:32:13'),(6,57,'Ad dolor ipsa error eum soluta deserunt in nesciunt. Deserunt quo dolores harum. Voluptate sit neque pariatur voluptate occaecati aut.','https://simonis.com/id-autem-quia-et-id.html','prompt',NULL,'2018-12-22 22:32:14','2018-12-22 22:32:14'),(7,4,'Vel labore numquam ducimus sit amet asperiores quas. Occaecati fugit vel aut exercitationem omnis repellendus. Rerum omnis quis deserunt facilis. Qui impedit nihil cum aliquid.','http://windler.com/enim-vel-et-quis-aut','urgent','2018-12-16 18:05:00','2018-12-22 22:32:14','2018-12-22 22:32:14'),(8,18,'Iusto in consequatur non adipisci et esse. Qui ad iste dolorem sint. Et aut commodi autem assumenda.','http://bradtke.com/','info',NULL,'2018-12-22 22:32:14','2018-12-22 22:32:14'),(9,38,'Accusamus placeat et sit. Doloremque quia odit ut modi harum fugit alias. Consectetur hic et sunt eaque et. Aut ipsum eligendi est.','http://gerlach.com/odit-earum-culpa-quia-nemo-magni-aliquam.html','info',NULL,'2018-12-22 22:32:14','2018-12-22 22:32:14'),(10,86,'Quis voluptates eius nisi quas quis quo. Aut harum nihil consequuntur consequuntur aut voluptas. Fuga dolores adipisci dignissimos asperiores.','https://frami.com/voluptate-quam-officiis-ratione.html','prompt',NULL,'2018-12-22 22:32:14','2018-12-22 22:32:14');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reactions`
--

DROP TABLE IF EXISTS `reactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reactor_id` int(11) NOT NULL,
  `reactable_id` int(11) NOT NULL,
  `reactable_model` varchar(50) NOT NULL,
  `type` enum('like','love','sad','angry') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reactions_1_idx` (`reactor_id`),
  CONSTRAINT `fk_reactions_1` FOREIGN KEY (`reactor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reactions`
--

LOCK TABLES `reactions` WRITE;
/*!40000 ALTER TABLE `reactions` DISABLE KEYS */;
INSERT INTO `reactions` VALUES (1,77,24,'App\\Message','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(2,11,37,'App\\Message','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(3,57,16,'App\\Comment','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(4,66,3,'App\\Message','angry','2018-12-22 22:32:13','2018-12-22 22:32:13'),(5,87,82,'App\\Media','love','2018-12-22 22:32:13','2018-12-22 22:32:13'),(6,28,11,'App\\Comment','angry','2018-12-22 22:32:13','2018-12-22 22:32:13'),(7,38,68,'App\\Message','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(8,63,89,'App\\Comment','like','2018-12-22 22:32:13','2018-12-22 22:32:13'),(9,71,78,'App\\Message','like','2018-12-22 22:32:13','2018-12-22 22:32:13'),(10,61,4,'App\\Media','angry','2018-12-22 22:32:13','2018-12-22 22:32:13'),(11,67,38,'App\\Message','angry','2018-12-22 22:32:13','2018-12-22 22:32:13'),(12,73,79,'App\\Comment','like','2018-12-22 22:32:13','2018-12-22 22:32:13'),(13,3,81,'App\\Comment','like','2018-12-22 22:32:13','2018-12-22 22:32:13'),(14,68,62,'App\\Message','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(15,80,3,'App\\Comment','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(16,32,72,'App\\Comment','love','2018-12-22 22:32:13','2018-12-22 22:32:13'),(17,23,88,'App\\Media','angry','2018-12-22 22:32:13','2018-12-22 22:32:13'),(18,95,85,'App\\Message','like','2018-12-22 22:32:13','2018-12-22 22:32:13'),(19,27,70,'App\\Media','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(20,81,51,'App\\Comment','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(21,24,59,'App\\Comment','like','2018-12-22 22:32:13','2018-12-22 22:32:13'),(22,89,90,'App\\Message','angry','2018-12-22 22:32:13','2018-12-22 22:32:13'),(23,90,82,'App\\Message','love','2018-12-22 22:32:13','2018-12-22 22:32:13'),(24,52,32,'App\\Comment','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(25,96,67,'App\\Media','like','2018-12-22 22:32:13','2018-12-22 22:32:13'),(26,95,8,'App\\Message','angry','2018-12-22 22:32:13','2018-12-22 22:32:13'),(27,9,3,'App\\Media','sad','2018-12-22 22:32:13','2018-12-22 22:32:13'),(28,70,100,'App\\Comment','angry','2018-12-22 22:32:13','2018-12-22 22:32:13'),(29,6,70,'App\\Media','like','2018-12-22 22:32:13','2018-12-22 22:32:13'),(30,81,17,'App\\Media','love','2018-12-22 22:32:13','2018-12-22 22:32:13');
/*!40000 ALTER TABLE `reactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `fk_role_user_2_idx` (`user_id`),
  CONSTRAINT `fk_role_user_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_user_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23),(1,24),(1,25),(1,26),(1,27),(1,28),(1,29),(1,30),(1,31),(1,32),(1,33),(1,34),(1,35),(1,36),(1,37),(1,38),(1,39),(1,40),(1,41),(1,42),(1,43),(1,44),(1,45),(1,46),(1,47),(1,48),(1,49),(1,50),(1,51),(1,52),(1,53),(1,54),(1,55),(1,56),(1,57),(1,58),(1,59),(1,60),(1,61),(1,62),(1,63),(1,64),(1,65),(1,66),(1,67),(1,68),(1,69),(1,70),(1,71),(1,72),(1,73),(1,74),(1,75),(1,76),(1,77),(1,78),(1,79),(1,80),(1,81),(1,82),(1,83),(1,84),(1,85),(1,86),(1,87),(1,88),(1,89),(1,90),(1,91),(1,92),(1,93),(1,94),(1,95),(1,96),(1,97),(1,98),(1,99),(1,100);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'user'),(2,'customer_service'),(3,'it_admin'),(4,'marketing_admin');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `screenshots`
--

DROP TABLE IF EXISTS `screenshots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `screenshots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `screenshooter_id` int(11) NOT NULL,
  `screenshotable_id` int(11) NOT NULL,
  `screenshotable_model` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_screenshots_1_idx` (`screenshooter_id`),
  CONSTRAINT `fk_screenshots_1` FOREIGN KEY (`screenshooter_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `screenshots`
--

LOCK TABLES `screenshots` WRITE;
/*!40000 ALTER TABLE `screenshots` DISABLE KEYS */;
INSERT INTO `screenshots` VALUES (1,16,29,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(2,73,5,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(3,70,65,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(4,48,25,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(5,76,90,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(6,11,40,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(7,76,79,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(8,60,41,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(9,92,65,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(10,60,55,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(11,50,87,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(12,40,9,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(13,47,55,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(14,61,98,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(15,71,10,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(16,42,69,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(17,57,26,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(18,13,99,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(19,88,75,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(20,97,4,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(21,44,66,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(22,17,63,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(23,49,48,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(24,7,66,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(25,15,18,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(26,96,65,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(27,89,7,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(28,49,76,'App\\Message','2018-12-22 22:32:13','2018-12-22 22:32:13'),(29,35,85,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13'),(30,24,14,'App\\Media','2018-12-22 22:32:13','2018-12-22 22:32:13');
/*!40000 ALTER TABLE `screenshots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suggestions`
--

DROP TABLE IF EXISTS `suggestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suggestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `suggester_id` int(11) NOT NULL,
  `suggestee_id` int(11) NOT NULL,
  `rank` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_suggestions_1_idx` (`suggester_id`),
  KEY `fk_suggestions_2_idx` (`suggestee_id`),
  CONSTRAINT `fk_suggestions_1` FOREIGN KEY (`suggester_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_suggestions_2` FOREIGN KEY (`suggestee_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suggestions`
--

LOCK TABLES `suggestions` WRITE;
/*!40000 ALTER TABLE `suggestions` DISABLE KEYS */;
INSERT INTO `suggestions` VALUES (1,52,87,11,'2018-12-22 22:32:12','2018-12-22 22:32:12'),(2,39,43,73,'2018-12-22 22:32:12','2018-12-22 22:32:12'),(3,31,53,29,'2018-12-22 22:32:12','2018-12-22 22:32:12'),(4,67,83,98,'2018-12-22 22:32:12','2018-12-22 22:32:12'),(5,11,45,68,'2018-12-22 22:32:12','2018-12-22 22:32:12'),(6,62,74,41,'2018-12-22 22:32:12','2018-12-22 22:32:12'),(7,46,67,79,'2018-12-22 22:32:13','2018-12-22 22:32:13'),(8,43,57,13,'2018-12-22 22:32:13','2018-12-22 22:32:13'),(9,53,76,69,'2018-12-22 22:32:13','2018-12-22 22:32:13'),(10,27,35,67,'2018-12-22 22:32:13','2018-12-22 22:32:13');
/*!40000 ALTER TABLE `suggestions` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `labyrinth_db`.`suggestions_BEFORE_INSERT` BEFORE INSERT ON `suggestions` FOR EACH ROW
BEGIN
	IF ( NEW.rank < 0 OR NEW.rank > 100 ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: rank should be in range [0, 100]';
	END IF;
    
    IF ( NEW.suggester_id = NEW.suggestee_id ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: suggester_id should be different from suggestee_id';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `labyrinth_db`.`suggestions_BEFORE_UPDATE` BEFORE UPDATE ON `suggestions` FOR EACH ROW
BEGIN
	IF ( NEW.rank < 0 OR NEW.rank > 100 ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: rank should be in range [0, 100]';
	END IF;
    
    IF ( NEW.suggester_id = NEW.suggestee_id ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: suggester_id should be different from suggestee_id';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `suggestions_to_connections_view`
--

DROP TABLE IF EXISTS `suggestions_to_connections_view`;
/*!50001 DROP VIEW IF EXISTS `suggestions_to_connections_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `suggestions_to_connections_view` (
  `connection_id` tinyint NOT NULL,
  `connection_created_at` tinyint NOT NULL,
  `connections_updated_at` tinyint NOT NULL,
  `suggestion_id` tinyint NOT NULL,
  `suggester_id` tinyint NOT NULL,
  `suggestee_id` tinyint NOT NULL,
  `rank` tinyint NOT NULL,
  `suggestion_created_at` tinyint NOT NULL,
  `suggestion_updated_at` tinyint NOT NULL,
  `request_id` tinyint NOT NULL,
  `requester_id` tinyint NOT NULL,
  `requestee_id` tinyint NOT NULL,
  `accepted_at` tinyint NOT NULL,
  `rejected_at` tinyint NOT NULL,
  `request_created_at` tinyint NOT NULL,
  `request_updated_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `total_connections_by_sex_view`
--

DROP TABLE IF EXISTS `total_connections_by_sex_view`;
/*!50001 DROP VIEW IF EXISTS `total_connections_by_sex_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `total_connections_by_sex_view` (
  `COUNT(*)` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `total_medias_uploaded_by_sex_view`
--

DROP TABLE IF EXISTS `total_medias_uploaded_by_sex_view`;
/*!50001 DROP VIEW IF EXISTS `total_medias_uploaded_by_sex_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `total_medias_uploaded_by_sex_view` (
  `COUNT(*)` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `total_messages_sent_by_sex_view`
--

DROP TABLE IF EXISTS `total_messages_sent_by_sex_view`;
/*!50001 DROP VIEW IF EXISTS `total_messages_sent_by_sex_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `total_messages_sent_by_sex_view` (
  `COUNT(*)` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `total_screenshots_taken_by_sex_view`
--

DROP TABLE IF EXISTS `total_screenshots_taken_by_sex_view`;
/*!50001 DROP VIEW IF EXISTS `total_screenshots_taken_by_sex_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `total_screenshots_taken_by_sex_view` (
  `COUNT(*)` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `total_users_by_sex_view`
--

DROP TABLE IF EXISTS `total_users_by_sex_view`;
/*!50001 DROP VIEW IF EXISTS `total_users_by_sex_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `total_users_by_sex_view` (
  `COUNT(*)` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `sex` enum('male','female','other') NOT NULL,
  `born_at` datetime NOT NULL,
  `educational_rank` varchar(45) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `current_country` varchar(255) DEFAULT NULL,
  `current_city` varchar(255) DEFAULT NULL COMMENT '		',
  `current_gps_coordinates` text DEFAULT NULL COMMENT 'JSON field: {"lat":"...", "lng":"..."}',
  `target_sex` enum('male','female','other') NOT NULL,
  `target_age_minimum` tinyint(4) DEFAULT NULL,
  `target_age_maximum` tinyint(4) DEFAULT NULL,
  `target_educational_rank` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Haleigh','Nitzsche','other','1964-04-10 00:51:48','researcher','imani.boyer@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Djibouti','West Jeradport','{\"lat\":21.093014,\"lng\":94.374387}','other',53,56,'university','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','bx6myUPLo7'),(2,'Damion','Ritchie','other','1955-07-01 23:53:15','postgraduate','ybecker@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Japan','Hermistonfort','{\"lat\":62.698735,\"lng\":45.891286}','male',58,69,'postgraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','3E8mSW9xPv'),(3,'Ocie','Lang','female','1952-02-18 21:53:42','undergraduate','amaya.barrows@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','San Marino','Kieraland','{\"lat\":85.074074,\"lng\":-42.826636}','female',18,44,'postgraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','YftJ0MdgPV'),(4,'Orville','Kertzmann','male','1970-05-26 20:56:37','undergraduate','qstamm@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Kazakhstan','East Jesus','{\"lat\":12.34482,\"lng\":-67.714173}','male',40,52,'undergraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','ZLDyBXIfdM'),(5,'Lucinda','Schulist','other','1989-03-04 23:30:12','undergraduate','deshawn15@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Sweden','Lake Flaviostad','{\"lat\":27.168123,\"lng\":44.577082}','male',52,56,'postgraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','3ThcJK74pT'),(6,'Morris','Berge','male','1949-07-18 09:37:20','university','lowe.elta@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Sierra Leone','Lorenborough','{\"lat\":-51.125335,\"lng\":-138.079172}','other',68,68,'university','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','MLxEBpD5FO'),(7,'Clinton','Hane','male','1980-02-11 12:27:44','researcher','wilton.wolff@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Norway','North Tristianhaven','{\"lat\":2.8693,\"lng\":-118.309141}','other',50,62,'researcher','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','Wk3w8luS5z'),(8,'Devin','Lockman','female','1977-06-13 07:10:55','postgraduate','liza.doyle@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Papua New Guinea','East Darianaland','{\"lat\":67.385395,\"lng\":162.218459}','other',54,62,'undergraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','HD2rXwkPdX'),(9,'Gudrun','Homenick','male','1954-04-19 14:57:29','undergraduate','allene.douglas@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Kuwait','VonRuedenbury','{\"lat\":-50.302755,\"lng\":-55.929494}','other',50,57,'researcher','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','juTskmMdQ5'),(10,'Myrtie','Balistreri','female','1968-07-12 18:21:00','undergraduate','wwelch@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Ethiopia','Delphiachester','{\"lat\":-58.773096,\"lng\":111.914411}','female',38,40,'undergraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','0LpYz5JYqf'),(11,'Jeromy','Davis','male','1980-07-12 04:10:23','postgraduate','kasandra35@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Guadeloupe','Port Demarcotown','{\"lat\":14.717229,\"lng\":10.358334}','female',48,59,'undergraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','BBX0xhIuaM'),(12,'Vincent','Macejkovic','female','1952-09-03 16:04:51','university','brice.ullrich@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Georgia','West Nicholasmouth','{\"lat\":29.734131,\"lng\":-49.486236}','female',18,64,'researcher','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','pEvg3V5hki'),(13,'Shanelle','Feest','male','1970-05-31 05:34:03','non-university','aglae48@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Argentina','New Kraig','{\"lat\":-70.443015,\"lng\":110.066947}','female',46,52,'researcher','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','DWQ4zCUMUC'),(14,'Elinore','Johnston','male','1949-06-27 10:06:44','postgraduate','willms.letha@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Honduras','West Margueriteville','{\"lat\":83.254023,\"lng\":-26.301955}','male',38,38,'university','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','YlxZ8kLe7e'),(15,'Tracey','Hegmann','female','1992-07-06 16:12:12','undergraduate','carolyn29@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Mayotte','Lake Mosesmouth','{\"lat\":37.94771,\"lng\":-49.434374}','female',22,42,'postgraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','Gn33r5Mjf9'),(16,'Katherine','Swift','female','1996-01-11 19:09:22','postgraduate','frieda.friesen@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Bangladesh','Garthview','{\"lat\":-34.056175,\"lng\":23.3556}','male',42,63,'postgraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','cJnIXbWtU1'),(17,'Elvera','Kozey','male','1949-05-05 05:45:44','researcher','wallace.upton@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Reunion','New Makenzieland','{\"lat\":7.123791,\"lng\":26.660462}','female',62,63,'university','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','rkOxctNyOG'),(18,'Sterling','Hayes','other','1992-09-03 00:45:01','researcher','guido.nader@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Central African Republic','North Roderickborough','{\"lat\":45.707612,\"lng\":-169.86756}','female',58,68,'undergraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','2CIEZKMwqa'),(19,'Antonietta','Bayer','male','1995-09-01 15:15:18','non-university','herminia.huels@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Montenegro','New Palmafort','{\"lat\":67.653816,\"lng\":-129.931651}','other',47,48,'postgraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','mxROU4rHN8'),(20,'Nayeli','Rath','other','2000-10-02 06:21:30','researcher','eloisa.tromp@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Central African Republic','Port Deven','{\"lat\":-62.138704,\"lng\":155.45312}','other',50,59,'researcher','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','aimlsq8qVa'),(21,'Danika','Kassulke','male','1980-05-14 01:06:14','researcher','smaggio@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Liechtenstein','Lake Noeliaview','{\"lat\":69.509377,\"lng\":-7.710282}','male',47,70,'postgraduate','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','ln8tIYlY13'),(22,'Shayne','Boyle','other','1996-09-22 17:56:21','undergraduate','zwillms@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Fiji','South Anastacio','{\"lat\":39.102094,\"lng\":-75.842861}','female',32,68,'university','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','fY2mCz4hW8'),(23,'Ola','Fritsch','female','1985-05-01 10:40:03','postgraduate','cassin.sean@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Finland','West Taliafurt','{\"lat\":43.44166,\"lng\":98.380629}','male',34,55,'non-university','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','TIDodBwmaA'),(24,'Alana','Fritsch','female','1973-06-07 02:53:12','non-university','kessler.eliseo@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Iceland','Jaskolskitown','{\"lat\":-65.940288,\"lng\":-90.976086}','other',18,33,'non-university','2018-12-22 22:32:10','2018-12-22 22:32:10','2018-12-22 22:32:10','5qjlH30L2C'),(25,'Ewell','Prohaska','other','1991-07-24 12:14:23','undergraduate','ward.keith@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Malaysia','North Bernadettefurt','{\"lat\":-40.528018,\"lng\":-105.356853}','other',58,58,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','bxRFDhgi3w'),(26,'Sienna','Gerlach','male','1966-07-20 03:54:06','non-university','turcotte.tracey@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Mali','Mohammadchester','{\"lat\":-33.506246,\"lng\":-178.32002}','male',19,51,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','0Bd1jglZjW'),(27,'Rosemary','Cole','female','2000-07-26 05:40:44','undergraduate','margie.marvin@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Liberia','East Linnea','{\"lat\":-24.782712,\"lng\":-137.158127}','female',36,45,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','cf14Hx8sZL'),(28,'Geovanny','Orn','male','1984-08-24 22:04:46','undergraduate','wgleason@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Turks and Caicos Islands','Port Andy','{\"lat\":39.897803,\"lng\":67.365456}','male',52,58,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','89L0GVYHwJ'),(29,'Valentin','Gusikowski','female','1952-09-18 01:53:42','university','yhammes@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Seychelles','Eunicechester','{\"lat\":-6.146394,\"lng\":37.002147}','male',34,49,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','3r1JGdW0nY'),(30,'Orin','Simonis','female','1982-05-06 21:58:50','non-university','gerlach.conor@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Honduras','Gudrunhaven','{\"lat\":4.459364,\"lng\":136.466359}','female',25,37,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','fHxtasrfxk'),(31,'Jailyn','Prosacco','female','1965-07-06 16:25:28','postgraduate','reyes06@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Svalbard & Jan Mayen Islands','Meganefort','{\"lat\":-86.712137,\"lng\":-127.29763}','male',60,62,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','Npl76L4PhH'),(32,'Grace','Schiller','other','1973-09-30 15:43:18','undergraduate','mireille08@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Anguilla','Terrystad','{\"lat\":-39.727122,\"lng\":-35.631616}','male',34,62,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','gqoFgAEoCO'),(33,'Aida','Predovic','other','1960-07-17 14:24:44','non-university','krajcik.dedric@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Algeria','West Troy','{\"lat\":-74.85356,\"lng\":138.592749}','male',29,69,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','UdvqRYrtYb'),(34,'Dayna','Douglas','female','1952-11-08 15:14:23','university','santino.treutel@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Saudi Arabia','Jacobsonport','{\"lat\":-37.740198,\"lng\":52.636508}','other',46,69,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','tY0cbeTI5B'),(35,'Colt','Will','female','1986-11-30 09:36:26','researcher','kuphal.levi@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Pitcairn Islands','New Damianview','{\"lat\":-24.942002,\"lng\":-126.55338}','male',53,70,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','gzKmQ3ne4U'),(36,'Walton','Osinski','male','1956-04-10 17:02:38','undergraduate','breitenberg.ana@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Thailand','North Isaiahtown','{\"lat\":19.867293,\"lng\":-163.095802}','other',36,56,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','nDTbHrgOcS'),(37,'Pink','Corwin','female','1998-03-22 23:16:38','undergraduate','jacynthe87@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Ethiopia','New Otisfort','{\"lat\":-11.581188,\"lng\":174.778057}','male',39,47,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','j4lB8iKWnS'),(38,'Dena','Balistreri','male','1959-03-20 04:06:47','undergraduate','roselyn22@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Bhutan','Lake Brennamouth','{\"lat\":-45.979347,\"lng\":139.470184}','male',64,66,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','QvY4JL1zOq'),(39,'Okey','Nikolaus','male','1956-03-28 08:53:59','undergraduate','gleichner.isabel@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Taiwan','Port Jillian','{\"lat\":-8.08438,\"lng\":-38.921903}','female',55,56,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','MHxtLgi8pI'),(40,'Jacquelyn','Daugherty','male','1951-01-26 22:40:14','researcher','clementina75@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Saint Martin','Audreanneshire','{\"lat\":-43.369617,\"lng\":-177.223366}','other',42,50,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','e5aeTIebzL'),(41,'Lucius','Tillman','female','1950-01-10 00:43:56','non-university','wlesch@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Bermuda','Jalontown','{\"lat\":14.781553,\"lng\":-25.910802}','female',29,51,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','kPiXvC7Yr4'),(42,'Micah','Goodwin','other','1961-07-15 02:57:15','researcher','candace81@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Belgium','Zemlakmouth','{\"lat\":-89.058667,\"lng\":78.21527}','other',29,43,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','wGQZbqeFnW'),(43,'Madisyn','Bayer','other','1977-08-10 10:05:04','non-university','sedrick.pagac@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Andorra','Lake Theron','{\"lat\":-28.833196,\"lng\":37.77278}','female',46,51,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','Mvxa4K81dU'),(44,'Cleveland','Berge','male','1986-10-23 14:42:40','undergraduate','jovani64@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Puerto Rico','Port Alphonso','{\"lat\":-38.701934,\"lng\":-70.665498}','female',20,43,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','rBqhNuNUHd'),(45,'Abigale','Heller','other','1983-03-12 02:50:01','researcher','theresia31@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Poland','Kassulketown','{\"lat\":33.963087,\"lng\":92.063565}','other',60,61,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','MPkg8knZzx'),(46,'Frieda','Konopelski','female','1955-05-30 02:55:16','researcher','hreynolds@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Sao Tome and Principe','Port Jeanetteview','{\"lat\":17.843213,\"lng\":151.98131}','other',20,25,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','LbQDqCkNib'),(47,'Arvel','Kreiger','female','1972-11-25 12:50:47','university','jordane.gusikowski@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Saint Pierre and Miquelon','Lake Lillianberg','{\"lat\":25.813201,\"lng\":-141.698834}','male',52,62,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','xAPAYSBjo9'),(48,'Maryse','Cole','other','1980-01-23 03:36:19','non-university','orin.weissnat@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Maldives','South Salvador','{\"lat\":-51.864432,\"lng\":-149.686345}','other',22,48,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','kb4x7UvcdL'),(49,'Marlee','Willms','male','1979-03-12 23:30:35','researcher','marlen00@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Norfolk Island','East Willieview','{\"lat\":77.502051,\"lng\":-95.484691}','female',62,65,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','WAJLvhbu9F'),(50,'Elliot','Fadel','male','1955-09-05 07:16:39','university','otilia.considine@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Hungary','Lake Hobart','{\"lat\":69.280116,\"lng\":-107.429622}','other',50,62,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','r5M6KI6HuC'),(51,'Devon','Lang','other','1982-07-08 22:08:37','undergraduate','wmetz@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Afghanistan','West Pinkie','{\"lat\":33.57942,\"lng\":149.452628}','male',49,60,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','8x7GTHq82p'),(52,'Creola','Little','male','1992-07-04 14:06:56','university','davon.gusikowski@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Mexico','West Keanu','{\"lat\":-0.849322,\"lng\":-57.164294}','female',62,67,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','MxnOimK02W'),(53,'Miller','Schiller','male','1977-02-04 02:46:48','non-university','desiree79@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','United States Virgin Islands','Mayerfurt','{\"lat\":78.341167,\"lng\":-51.659445}','other',21,44,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','5q08uLSzK1'),(54,'Trycia','Weber','female','1992-04-15 23:28:39','undergraduate','yritchie@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Namibia','Isaiahchester','{\"lat\":-18.723377,\"lng\":39.545118}','male',26,52,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','En68c4tHSh'),(55,'Lelah','Murphy','female','1966-10-24 07:57:56','non-university','collins.eleonore@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Bosnia and Herzegovina','Wuckertberg','{\"lat\":-78.376994,\"lng\":168.640256}','male',63,70,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','boBUxlClVW'),(56,'Clyde','Erdman','other','1957-11-14 05:12:05','postgraduate','vhilpert@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Saint Lucia','Wavaburgh','{\"lat\":17.992764,\"lng\":-52.356972}','female',37,56,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','QtFX9Rc7n6'),(57,'Christy','Larson','other','1983-06-22 10:25:04','undergraduate','warmstrong@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Botswana','Port Dorcas','{\"lat\":11.858439,\"lng\":127.263301}','female',60,70,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','ymlHaE7wxV'),(58,'Alvis','Jacobs','male','1978-06-25 02:28:33','university','jocelyn.gulgowski@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Switzerland','New Clementine','{\"lat\":29.617499,\"lng\":-93.337361}','female',20,34,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','MnXjjWOwXc'),(59,'Andrew','O\'Conner','female','1965-10-11 16:15:29','postgraduate','bethany30@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Qatar','Mohamedburgh','{\"lat\":-20.725972,\"lng\":115.28509}','male',64,67,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','5CvWvmy5Iv'),(60,'Jess','Schowalter','female','1961-03-30 18:44:44','university','zboncak.george@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Comoros','Wunschton','{\"lat\":75.018042,\"lng\":-165.236366}','other',60,69,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','L0WgYlFf1T'),(61,'Lionel','Mertz','other','1972-01-01 06:04:13','postgraduate','breitenberg.oda@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Nepal','North Kole','{\"lat\":-54.125964,\"lng\":101.011505}','other',34,55,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','Pi9qq8HwHl'),(62,'Erin','Rice','other','1971-11-18 12:20:56','undergraduate','adams.dorian@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Equatorial Guinea','North Carleton','{\"lat\":45.817393,\"lng\":-94.217207}','female',23,36,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','UTqRpglvzt'),(63,'Adalberto','Kulas','other','1957-12-29 14:24:36','undergraduate','kelsi.rempel@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Iran','Wiegandton','{\"lat\":-37.326834,\"lng\":-105.772355}','female',56,66,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','5TOs2VmU9G'),(64,'Cathy','Dach','male','1969-04-26 15:34:13','researcher','haag.alena@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Kenya','New Emmanuelmouth','{\"lat\":-5.893847,\"lng\":54.192929}','other',18,30,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','rSXFxX3d6a'),(65,'Meda','Fisher','female','1960-10-13 22:01:00','non-university','iliana.krajcik@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Solomon Islands','New Kitty','{\"lat\":-16.014835,\"lng\":-35.128615}','male',45,64,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','hsEFVLKHAZ'),(66,'Earnestine','Rutherford','female','1965-01-27 17:40:32','postgraduate','orlando.treutel@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Lithuania','East Ellie','{\"lat\":31.4993,\"lng\":8.076024}','male',52,64,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','gLrh08DqNK'),(67,'Solon','Deckow','female','1979-04-30 14:25:25','non-university','adenesik@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Eritrea','West Alexandrechester','{\"lat\":6.725399,\"lng\":-139.58751}','male',35,45,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','RC0xDIV6yU'),(68,'Violet','Kling','male','1990-09-29 12:59:39','undergraduate','jamil.hansen@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Albania','Coleport','{\"lat\":-16.052736,\"lng\":-155.824374}','male',19,42,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','iFwKgw7Grj'),(69,'Ella','Medhurst','other','1968-12-09 17:57:06','non-university','lind.murray@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Iran','Weldonport','{\"lat\":47.367255,\"lng\":174.006312}','other',23,40,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','DPqfqGFvHj'),(70,'Napoleon','Romaguera','male','1969-01-19 14:02:35','researcher','stiedemann.janelle@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Cape Verde','Jacobihaven','{\"lat\":39.755476,\"lng\":147.666504}','other',35,38,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','MfawfeJkgb'),(71,'Kobe','Abbott','male','1951-06-07 12:27:50','postgraduate','auer.abbie@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Bahamas','Hintzborough','{\"lat\":33.157587,\"lng\":70.822462}','female',18,35,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','CjHYxxbOCx'),(72,'Krystal','Harvey','female','1970-12-19 11:11:25','undergraduate','coberbrunner@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Svalbard & Jan Mayen Islands','Paulside','{\"lat\":16.540975,\"lng\":109.914114}','female',56,64,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','HaCx4alvr9'),(73,'Armando','Metz','female','1972-04-22 07:09:32','undergraduate','bednar.jadon@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Tanzania','Reynoldberg','{\"lat\":15.907005,\"lng\":-140.515834}','male',64,69,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','Cmwwiy8ITU'),(74,'Mariam','Brown','female','1960-08-15 17:38:29','researcher','jude86@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Uganda','South Ocie','{\"lat\":58.536482,\"lng\":-112.114103}','other',40,59,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','IOtVFz0Z98'),(75,'Nicole','Hayes','other','1954-06-27 14:29:40','university','jeffrey.gulgowski@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Angola','Flatleyview','{\"lat\":79.0633,\"lng\":158.176808}','female',45,55,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','Rsnph9O8aO'),(76,'Naomi','Gerlach','male','1959-07-28 07:23:16','university','brian.terry@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Singapore','Cristmouth','{\"lat\":2.390341,\"lng\":97.434051}','female',33,35,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','QXv7y6aI1o'),(77,'Julianne','Spinka','other','1988-05-16 11:27:33','university','kbergnaum@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Saint Pierre and Miquelon','North Chadmouth','{\"lat\":89.599946,\"lng\":147.811064}','female',65,67,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','7quyKIJ7dD'),(78,'Carmelo','Block','male','1999-08-23 10:48:53','non-university','trantow.bernard@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Vietnam','Tedchester','{\"lat\":43.837751,\"lng\":34.883576}','male',29,60,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','btJEEsUoOG'),(79,'Opal','O\'Conner','female','1983-04-25 12:21:11','researcher','jaqueline21@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Lithuania','West Reta','{\"lat\":-80.035266,\"lng\":15.884362}','other',44,60,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','xK2MWGRAb0'),(80,'Terrill','Block','male','1963-11-05 00:07:30','researcher','rbogisich@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Uruguay','Marquardtstad','{\"lat\":83.372012,\"lng\":13.07012}','male',67,67,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','vrg4Z27jDd'),(81,'Veronica','Marks','other','1964-04-30 23:58:55','undergraduate','stanton.jo@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Peru','Runolfssonside','{\"lat\":-11.949869,\"lng\":124.902489}','female',41,47,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','E0yzs9DGGq'),(82,'Santiago','Walsh','female','1991-11-03 20:27:36','non-university','tabitha.brakus@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Barbados','West Hassanshire','{\"lat\":-75.406206,\"lng\":129.731287}','other',53,66,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','YcXZaoeYGO'),(83,'Akeem','Labadie','other','1983-11-15 11:20:39','university','myrna08@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Guernsey','Mitchellfort','{\"lat\":-78.407675,\"lng\":91.358736}','male',27,27,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','igLxW81YW7'),(84,'Alana','Dare','male','1977-10-07 09:59:37','researcher','gabriel36@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Iran','East Orin','{\"lat\":-70.930057,\"lng\":-41.643338}','female',30,61,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','1gp1uhY5aV'),(85,'Thomas','Ortiz','female','1986-05-16 07:34:45','university','vdonnelly@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Algeria','West Alford','{\"lat\":60.484751,\"lng\":-97.134814}','other',55,60,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','vZCxjXhqbk'),(86,'Armand','Gerlach','male','1955-12-08 15:10:06','undergraduate','renner.millie@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','United Kingdom','Blairchester','{\"lat\":88.88151,\"lng\":-12.137785}','other',18,48,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','mxsY5nfbyX'),(87,'Kiara','Quitzon','other','1979-04-27 07:22:32','postgraduate','ywill@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Saint Kitts and Nevis','Gaylordmouth','{\"lat\":82.693379,\"lng\":-82.8135}','other',43,46,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','dbaR3KzuIM'),(88,'Alisha','Bogan','other','1989-12-06 14:44:56','researcher','salvador.rempel@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Libyan Arab Jamahiriya','Lake Tyratown','{\"lat\":-37.574451,\"lng\":-39.558992}','other',46,48,'postgraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','K2FvmbqZEk'),(89,'Daisy','Durgan','male','1990-06-30 17:45:37','undergraduate','kris53@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Monaco','Lake Laurel','{\"lat\":41.551365,\"lng\":127.943255}','female',46,47,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','k6f2gko9hJ'),(90,'Stacy','Wolff','male','1958-04-13 13:20:15','researcher','eddie90@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Netherlands Antilles','Earnesthaven','{\"lat\":40.575349,\"lng\":-164.413665}','female',24,39,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','cQkBF1zTim'),(91,'Irving','Rohan','female','1950-06-06 19:23:23','non-university','botsford.albertha@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Sri Lanka','Mitchellville','{\"lat\":-57.163632,\"lng\":124.701053}','other',56,68,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','65Td7CHh7B'),(92,'Ollie','Brown','male','1979-06-07 09:20:16','undergraduate','tianna12@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Iceland','West Masonview','{\"lat\":73.936915,\"lng\":-168.872943}','other',26,35,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','2S9UppWL7M'),(93,'Kiara','O\'Hara','female','1986-01-25 10:07:13','researcher','greg.dare@example.net','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Cote d\'Ivoire','Emardshire','{\"lat\":-40.385089,\"lng\":169.373881}','female',51,53,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','B0azGOyQ3g'),(94,'Phyllis','Wintheiser','other','1954-06-11 11:03:18','undergraduate','sonny.kunze@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Poland','South Hazelview','{\"lat\":74.090324,\"lng\":73.629685}','male',50,60,'undergraduate','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','hqh6pMfo3W'),(95,'Friedrich','Ondricka','female','1970-07-17 14:39:52','researcher','dane.kuhlman@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Rwanda','Lake Barton','{\"lat\":-44.224523,\"lng\":-111.933175}','female',20,20,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','8a5ys31Q62'),(96,'Harmon','Mann','male','1965-09-16 13:12:25','university','ireichert@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Armenia','East Derickport','{\"lat\":-64.996894,\"lng\":-103.06115}','male',58,58,'non-university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','jHrLWkNY8U'),(97,'Brendon','Hirthe','male','1962-06-08 07:23:48','non-university','fbeahan@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Hong Kong','South Linaberg','{\"lat\":-60.894662,\"lng\":138.939537}','male',48,62,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','69NXaayT94'),(98,'Merle','Sawayn','other','1981-08-13 08:40:21','undergraduate','bergnaum.jamel@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Senegal','Willmouth','{\"lat\":9.224058,\"lng\":97.491149}','other',38,55,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','LVN6jDYyn0'),(99,'Kellie','Gleichner','female','1966-07-20 03:11:54','university','schoen.hertha@example.org','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Jamaica','Port Francis','{\"lat\":-4.940879,\"lng\":106.129032}','female',70,70,'researcher','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','jm7n8rIdWJ'),(100,'Tod','Doyle','male','1955-03-30 17:07:47','university','kelsi.donnelly@example.com','$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','Albania','East Zoie','{\"lat\":61.094315,\"lng\":169.817611}','male',53,58,'university','2018-12-22 22:32:11','2018-12-22 22:32:11','2018-12-22 22:32:10','RzuMPq4kjK');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `labyrinth_db`.`users_BEFORE_INSERT` BEFORE INSERT ON `users` FOR EACH ROW
BEGIN
	IF ( NEW.target_age_minimum < 18 OR NEW.target_age_maximum > 100 ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: target_age should be in [18, 100] years old';
	END IF;
    
    IF ( TIMESTAMPDIFF( YEAR, NEW.born_at, CURDATE() ) < 18 ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: born_at should produce age greater than or equal to 18 years old';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `labyrinth_db`.`users_BEFORE_UPDATE` BEFORE UPDATE ON `users` FOR EACH ROW
BEGIN
	IF ( NEW.target_age_minimum < 18 OR NEW.target_age_maximum > 100 ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: target_age should be in [18, 100] years old';
	END IF;
    
    IF ( TIMESTAMPDIFF( YEAR, NEW.born_at, CURDATE() ) < 18 ) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data: born_at should produce age greater than or equal to 18 years old';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `labyrinth_db`
--

USE `labyrinth_db`;

--
-- Final view structure for view `comment_reactions_view`
--

/*!50001 DROP TABLE IF EXISTS `comment_reactions_view`*/;
/*!50001 DROP VIEW IF EXISTS `comment_reactions_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `comment_reactions_view` AS select `reactions`.`id` AS `id`,`reactions`.`reactor_id` AS `reactor_id`,`reactions`.`reactable_id` AS `comment_id`,`reactions`.`type` AS `type`,`reactions`.`created_at` AS `created_at`,`reactions`.`updated_at` AS `updated_at` from `reactions` where `reactions`.`reactable_model` = 'App\\Comment' */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `media_comments`
--

/*!50001 DROP TABLE IF EXISTS `media_comments`*/;
/*!50001 DROP VIEW IF EXISTS `media_comments`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `media_comments` AS select `medias`.`id` AS `id`,`comments`.`body` AS `body`,`comments`.`created_at` AS `created_at` from (`medias` join `comments` on(`medias`.`id` = `comments`.`media_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `media_reactions_view`
--

/*!50001 DROP TABLE IF EXISTS `media_reactions_view`*/;
/*!50001 DROP VIEW IF EXISTS `media_reactions_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `media_reactions_view` AS select `reactions`.`id` AS `id`,`reactions`.`reactor_id` AS `reactor_id`,`reactions`.`reactable_id` AS `media_id`,`reactions`.`type` AS `type`,`reactions`.`created_at` AS `created_at`,`reactions`.`updated_at` AS `updated_at` from `reactions` where `reactions`.`reactable_model` = 'App\\Media' */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `media_screenshots_view`
--

/*!50001 DROP TABLE IF EXISTS `media_screenshots_view`*/;
/*!50001 DROP VIEW IF EXISTS `media_screenshots_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `media_screenshots_view` AS select `screenshots`.`id` AS `id`,`screenshots`.`screenshooter_id` AS `screenshooter_id`,`screenshots`.`screenshotable_id` AS `media_id`,`screenshots`.`created_at` AS `created_at`,`screenshots`.`updated_at` AS `updated_at` from `screenshots` where `screenshots`.`screenshotable_model` = 'App\\Media' */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `message_reactions_view`
--

/*!50001 DROP TABLE IF EXISTS `message_reactions_view`*/;
/*!50001 DROP VIEW IF EXISTS `message_reactions_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `message_reactions_view` AS select `reactions`.`id` AS `id`,`reactions`.`reactor_id` AS `reactor_id`,`reactions`.`reactable_id` AS `message_id`,`reactions`.`type` AS `type`,`reactions`.`created_at` AS `created_at`,`reactions`.`updated_at` AS `updated_at` from `reactions` where `reactions`.`reactable_model` = 'App\\Message' */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `message_screenshots_view`
--

/*!50001 DROP TABLE IF EXISTS `message_screenshots_view`*/;
/*!50001 DROP VIEW IF EXISTS `message_screenshots_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `message_screenshots_view` AS select `screenshots`.`id` AS `id`,`screenshots`.`screenshooter_id` AS `screenshooter_id`,`screenshots`.`screenshotable_id` AS `message_id`,`screenshots`.`created_at` AS `created_at`,`screenshots`.`updated_at` AS `updated_at` from `screenshots` where `screenshots`.`screenshotable_model` = 'App\\Message' */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `suggestions_to_connections_view`
--

/*!50001 DROP TABLE IF EXISTS `suggestions_to_connections_view`*/;
/*!50001 DROP VIEW IF EXISTS `suggestions_to_connections_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `suggestions_to_connections_view` AS select `connections`.`id` AS `connection_id`,`connections`.`created_at` AS `connection_created_at`,`connections`.`updated_at` AS `connections_updated_at`,`suggestions`.`id` AS `suggestion_id`,`suggestions`.`suggester_id` AS `suggester_id`,`suggestions`.`suggestee_id` AS `suggestee_id`,`suggestions`.`rank` AS `rank`,`suggestions`.`created_at` AS `suggestion_created_at`,`suggestions`.`updated_at` AS `suggestion_updated_at`,`connection_requests`.`id` AS `request_id`,`connection_requests`.`requester_id` AS `requester_id`,`connection_requests`.`requestee_id` AS `requestee_id`,`connection_requests`.`accepted_at` AS `accepted_at`,`connection_requests`.`rejected_at` AS `rejected_at`,`connection_requests`.`created_at` AS `request_created_at`,`connection_requests`.`updated_at` AS `request_updated_at` from ((`suggestions` join `connection_requests` on(`suggestions`.`suggester_id` = `connection_requests`.`requester_id` and `suggestions`.`suggestee_id` = `connection_requests`.`requestee_id`)) join `connections` on(`connections`.`request_id` = `connection_requests`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `total_connections_by_sex_view`
--

/*!50001 DROP TABLE IF EXISTS `total_connections_by_sex_view`*/;
/*!50001 DROP VIEW IF EXISTS `total_connections_by_sex_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `total_connections_by_sex_view` AS select count(0) AS `COUNT(*)` from ((`connections` join `connection_requests` on(`connection_requests`.`id` = `connections`.`request_id`)) join `users` on(`users`.`id` = `connection_requests`.`requester_id`)) group by `users`.`sex` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `total_medias_uploaded_by_sex_view`
--

/*!50001 DROP TABLE IF EXISTS `total_medias_uploaded_by_sex_view`*/;
/*!50001 DROP VIEW IF EXISTS `total_medias_uploaded_by_sex_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `total_medias_uploaded_by_sex_view` AS select count(0) AS `COUNT(*)` from (`medias` join `users` on(`users`.`id` = `medias`.`uploader_id`)) group by `users`.`sex` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `total_messages_sent_by_sex_view`
--

/*!50001 DROP TABLE IF EXISTS `total_messages_sent_by_sex_view`*/;
/*!50001 DROP VIEW IF EXISTS `total_messages_sent_by_sex_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `total_messages_sent_by_sex_view` AS select count(0) AS `COUNT(*)` from (`messages` join `users` on(`users`.`id` = `messages`.`sender_id`)) group by `users`.`sex` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `total_screenshots_taken_by_sex_view`
--

/*!50001 DROP TABLE IF EXISTS `total_screenshots_taken_by_sex_view`*/;
/*!50001 DROP VIEW IF EXISTS `total_screenshots_taken_by_sex_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `total_screenshots_taken_by_sex_view` AS select count(0) AS `COUNT(*)` from (`screenshots` join `users` on(`users`.`id` = `screenshots`.`screenshooter_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `total_users_by_sex_view`
--

/*!50001 DROP TABLE IF EXISTS `total_users_by_sex_view`*/;
/*!50001 DROP VIEW IF EXISTS `total_users_by_sex_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `total_users_by_sex_view` AS select count(0) AS `COUNT(*)` from `users` group by `users`.`sex` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-22 22:33:05
