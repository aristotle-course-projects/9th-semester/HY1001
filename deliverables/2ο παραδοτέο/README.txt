-------------------------------------------------
- Directories
------------------------------------------------
This assignment includes the following directories:
  - [1_model]
    Contains the MySql Workbench(R) exported model. This model contains the tables, views and E/R diagram for LabyrinthDB.
    Also, a PNG image is inluded showing the final E/R diagram ( exported from MySQL Workbench(R) ).
  - [2_users]
    Contains the SQL scripts to create the necessary db roles and db users for LabyrinthDB.
  - [3_queries]
    Contains sample query-scripts that will be made to db once the web app comes to life.
  - [4_dump]
    Contains the dump for creating the database (tables, constraints, inserted data, views)

-------------------------------------------------
- Files
------------------------------------------------
This assignment includes the following files:
- [root]
  - LabyrinthDB_Del_1_revised.pdf: corrected 1st deliverable of the assignment.
- [1_model]
  - lbdb_model.mwb
  - lbdb_model_er.png
- [2_users]
  - lbdb_roles.sql  
  - lbdb_users.sql
- [3_queries]
  - lbdb_q1.sql: "ÎÏÏÏ ÏÏÎ¹ ÎµÏÎ¹Î¸ÏÎ¼Î¿ÏÎ¼Îµ Î³Î¹Î± Î­Î½Î±Î½ ÏÏÎ³ÎºÎµÎºÏÎ¹Î¼Î­Î½Î¿ ÏÏÎ®ÏÏÎ· Î½Î± Î´Î¿ÏÎ¼Îµ ÏÎ¿ÏÏ ÏÏÎ¿ÏÎµÎ¹Î½ÏÎ¼ÎµÎ½Î¿ÏÏ ÏÎµ Î±ÏÏÏÎ½ ÏÏÎ®ÏÏÎµÏ Î±ÏÏ ÏÎ·Î½ ÎµÏÎ±ÏÎ¼Î¿Î³Î®"
  - lbdb_q2.sql: "ÎÏÏÏ ÏÏÎ¹ ÎµÏÎ¹Î¸ÏÎ¼Î¿ÏÎ¼Îµ Î½Î± Î´Î¿ÏÎ¼Îµ ÏÎ¿ÏÏ ÏÏÎ»Î¿ÏÏ ÎµÎ½ÏÏ ÏÏÎ³ÎºÎµÎºÏÎ¹Î¼Î­Î½Î¿Ï ÏÏÎ®ÏÏÎ· ÏÏÎ·Î½ ÎµÏÎ±ÏÎ¼Î¿Î³Î® ( Î±ÏÎ»ÏÏ ÏÏÎ®ÏÏÎ·Ï, Î´Î¹Î±ÏÎµÎ¹ÏÎ¹ÏÏÎ®Ï ÎºÎ»Ï )"
  - lbdb_q3.sql: "ÎÏÏÏ ÏÏÎ¹ ÎµÏÎ¹Î¸ÏÎ¼Î¿ÏÎ¼Îµ Î½Î± Î´Î¿ÏÎ¼Îµ ÏÎ¿ÏÏ ÏÏÎ®ÏÏÎµÏ ÏÎ¿Ï Î­ÎºÎ±Î½Î±Î½ screenshot ÏÎµ ÏÏÎ³ÎºÎµÎºÏÎ¹Î¼Î­Î½Î¿ media ( Ï.Ï. media Î¼Îµ Î±ÏÎ¹Î¸Î¼Ï id 5 ) ÎµÎ½ÏÏ ÏÏÎ®ÏÏÎ·"
  - lbdb_q4.sql: "ÎÏÏÏ ÏÏÎ¹ ÎµÏÎ¹Î¸ÏÎ¼Î¿ÏÎ¼Îµ Î½Î± Î´Î¿ÏÎ¼Îµ ÏÎ¿ÏÏ ÏÏÎ®ÏÏÎµÏ ÏÎ¿Ï Î­ÎºÎ±Î½Î±Î½ ÎµÎ¯ÏÎµ âloveâ ÎµÎ¯ÏÎµ âangryâ ÏÎµ Î­Î½Î± media ( Ï.Ï. Î±ÏÏÎ¿Ï Î¼Îµ id 5 ) ÎºÎ¬ÏÎ¿Î¹Î¿Ï ÏÏÎ®ÏÏÎ·"
  - lbdb_q5.sql: "ÎÏÏÏ ÏÏÎ¹ Î¸Î­Î»Î¿ÏÎ¼Îµ Î½Î± Î´Î¿ÏÎ¼Îµ ÏÎ± Î¼Î·Î½ÏÎ¼Î±ÏÎ± Î¼ÎµÏÎ±Î¾Ï Î´ÏÎ¿ ÏÏÎ·ÏÏÏÎ½ ÎºÎ±Î¸ÏÏ ÎºÎ±Î¹ ÏÎ¹Ï Î·Î¼ÎµÏÎ¿Î¼Î·Î½Î¯ÎµÏ ÏÏÎ¹Ï Î¿ÏÎ¿Î¯ÎµÏ ÏÏÎ¬Î»Î¸Î·ÎºÎ±Î½ ( ÏÏÎ¿Î¸Î­ÏÎ¿ÏÎ¼Îµ ÏÏÎ¹ Î· ÏÏÎ½Î¿Î¼Î¹Î»Î¯Î± ÎµÎ¯Î½Î±Î¹ Î¼ÎµÏÎ±Î¾Ï ÏÎ¿Ï ÏÏÎ®ÏÏÎ· Î¼Îµ id 3 ÎºÎ±Î¹ Î±ÏÏÎ¿Ï Î¼Îµ id 5 )"
- [4_dump]
  - lbdb_dump.sql

